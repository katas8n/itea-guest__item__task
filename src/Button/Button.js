import React, { Component } from 'react';

import '../Button/Button.css';

class Button extends Component {
    render() {
        const { arrived, toggleHendler } = this.props;
        return (
            <button
                onClick={toggleHendler}
                className={
                    arrived
                        ? 'btn btn-outline-success d-flex col-2'
                        : 'btn btn-outline-danger d-flex col-2'
                }
            >
                {arrived ? 'Прибыл' : 'Отсутствует'}
            </button>
        );
    }
}

export default Button;
