import React, { Component } from 'react';

import Button from '../Button';

import './GuestItem.css';

class GuestItem extends Component {
    render() {
        const { guestData, arrived, toggleHendler } = this.props;

        return (
            <div className="d-flex">
                <ul className={arrived ? 'col-6 app-done' : 'col-6'}>
                    <li>
                        Гость: <strong>{guestData.name}</strong> , работает в
                        компании:
                        <strong>{guestData.company}</strong>
                        <p />
                        Его контакты :<strong>E-mail:{guestData.email},</strong>
                        <p />
                        <strong> {guestData.address}</strong>
                        <strong>, {guestData.phone} </strong>
                    </li>
                </ul>
                <Button arrived={arrived} toggleHendler={toggleHendler} />
            </div>
        );
    }
}

export default GuestItem;
