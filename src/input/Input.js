import React, { Component } from 'react';

import './Input.css';

class Input extends Component {
    render() {
        const { value, changeHandler, done } = this.props;
        return (
            <div className="d-flex input__wrapper">
                <input
                    className="input"
                    placeholder="search"
                    value={value}
                    onChange={changeHandler}
                />
                <h4 className="done">Guests : {done}</h4>
            </div>
        );
    }
}

export default Input;
