import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';

import './App.css';

import GuestItem from '../GuestItem/GuestItem';
import Input from '../input/Input';

import Guests from '../Guests/guests.json';

const arrivedArr = Guests.map(el => {
    return {
        el,
        arrived: false
    };
});

class App extends Component {
    state = {
        guests: arrivedArr.slice(0, 9),
        done: arrivedArr.slice(0, 9).length,
        value: '',
        filltredGuests: [],
        sliceIndex: 9
    };

    allUsers = () => {
        this.setState({
            guests: arrivedArr
        });
    };

    addButtonHandler = () => {
        const newIndex = this.state.sliceIndex + 10;

        this.setState({
            sliceIndex: newIndex,
            guests: arrivedArr.slice(0, newIndex),
            done: arrivedArr.slice(0, newIndex).length + 1
        });
    };

    onHendlerToggle = index => e => {
        const guestState = this.state.guests;
        const arrivedUsers = guestState.map(guest => {
            if (guest.el.index === index) {
                guest.arrived = !guest.arrived;
            }
            return guest;
        });
        this.setState({ guests: arrivedUsers });
    };

    changeHandler = e => {
        const query = e.target.value.toLowerCase();
        const filtredUsers = this.state.guests.filter(guest => {
            let name = guest.el.name.toLowerCase();
            let company = guest.el.company.toLowerCase();
            let email = guest.el.email.toLowerCase();
            let phone = guest.el.phone.toLowerCase();
            let address = guest.el.address.toLowerCase();

            if (
                name.indexOf(query) !== -1 ||
                company.indexOf(query) !== -1 ||
                email.indexOf(query) !== -1 ||
                phone.indexOf(query) !== -1 ||
                address.indexOf(query) !== -1
            ) {
                return true;
            } else {
                return false;
            }
        });

        this.setState({
            value: query,
            filltredGuests: filtredUsers,
            done: filtredUsers.length
        });
    };

    render() {
        const { guests, value, filltredGuests, done, sliceIndex } = this.state;

        let data = guests;

        if (filltredGuests.length > 0) {
            data = filltredGuests;
        }
        return (
            <Fragment>
                <Input
                    value={value}
                    changeHandler={this.changeHandler}
                    done={done}
                />
                {data.map(({ el, arrived }) => (
                    <GuestItem
                        key={el._id}
                        guestData={el}
                        arrived={arrived}
                        toggleHendler={this.onHendlerToggle(el.index)}
                    />
                ))}

                {sliceIndex < arrivedArr.length && (
                    <button
                        type="button"
                        onClick={this.addButtonHandler}
                        theme="add"
                        className="button btn btn-outline-primary col-2"
                    >
                        Add
                    </button>
                )}
            </Fragment>
        );
    }
}

export default App;
